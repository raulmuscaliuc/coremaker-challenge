# Coremaker challenge

After cloning the project on your local machine follow these steps:

- open a terminal in coremaker folder
- run 'npm install' command
- run 'npm start'

Your project should start on http://localhost:3000.
Google client id is available for the next ports:

- 3000
- 3001
- 5000
