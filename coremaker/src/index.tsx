import ReactDOM from 'react-dom'
import App from './App'
import reportWebVitals from './reportWebVitals'
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom'
import jwt_decode from 'jwt-decode'
import { createStore } from 'redux'
import { appReducer } from './store/reducers/reducer'
import { Provider, useDispatch, useSelector } from 'react-redux'
import { AppState, User, TokenInfo } from './store'
import Profile from './components/profile/Profile'
import { setUserData } from './store/actions/actions'

const PrivateRoute = (props: any) => {
	const dispatch = useDispatch()
	const { children } = props
	const tokenInfo: TokenInfo = useSelector((state: AppState) => state.tokenInfo)
	if (tokenInfo.token === '') return <Navigate to='/login' />

	try {
		const decodedToken: any = jwt_decode(tokenInfo.id_token)
		const { email, family_name, given_name, picture } = decodedToken
		const user: User = { email, family_name, given_name, picture }
		dispatch(setUserData(user))
	} catch (e) {
		console.log('Eroare,', e)
		return <Navigate to='/login' />
	}

	return <div>{children}</div>
}

const store = createStore(appReducer)

ReactDOM.render(
	<Provider store={store}>
		<BrowserRouter>
			<Routes>
				<Route path='/login' element={<App />} />
				<Route
					path='/*'
					element={
						<PrivateRoute>
							<Routes>
								<Route path='/profile' element={<Profile />} />
							</Routes>
						</PrivateRoute>
					}
				/>
			</Routes>
		</BrowserRouter>
	</Provider>,
	document.getElementById('root')
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()