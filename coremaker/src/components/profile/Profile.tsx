import { GoogleLogout } from 'react-google-login'
import { useDispatch, useSelector } from 'react-redux'
import { AppState, GOOGLE_CLIENT_ID} from '../../store'
import { clearData } from '../../store/actions/actions'
import './Profile.css'

function Profile() {

	const dispatch = useDispatch()

	const profileInfo = useSelector((state: AppState) => state.user)

	const logout = () => {
		dispatch(clearData())
	}

	return (
		<div className='container'>
			<div className='details-box'>
				<div className='details-title-box'>
					<h3>Basic Details</h3>
				</div>
				<div className='details-info-box'>
					<div className='profile-info-box'>
						<p>profile image</p>
						<img className='profile-image' src={profileInfo?.picture} />
					</div>
					<div>
						<p>
							Full Name: {' '}
							<span>
								{profileInfo?.given_name} {profileInfo?.family_name}
							</span>
						</p>
						<p>
							Email: {' '}
							<span>{profileInfo?.email}</span>
						</p>
					</div>
					<div>
						<GoogleLogout
							clientId={GOOGLE_CLIENT_ID}
							buttonText="Logout"
							onLogoutSuccess={logout}
						>
						</GoogleLogout>
					</div>
				</div>
			</div>
		</div>
	)
}

export default Profile