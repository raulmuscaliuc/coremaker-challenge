export interface Action {
    type: string
    payload: any
}

export interface AppState {
    tokenInfo: TokenInfo
    user: User
}

export interface User {
    email: string,
    family_name: string,
    given_name: string,
    picture: string
}

export interface TokenInfo {
    token: string,
    id_token: string
}

export const GOOGLE_CLIENT_ID = '699404879099-jribo0f46b8dgo17n4v0k0i5khapuonk.apps.googleusercontent.com'