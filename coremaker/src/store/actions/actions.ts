import { TokenInfo, User } from ".."

export function login(token: TokenInfo) {
    return {
        type: 'SET_TOKEN_INFO',
        payload: token
    }
}

export function clearData() {
    return {
        type: 'CLEAR_DATA',
        payload: ''
    }
}

export function setUserData(user: User) {
    return {
        type: 'SET_USER_DATA',
        payload: user
    }
}