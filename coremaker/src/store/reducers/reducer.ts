import {Action, AppState} from '../index'

const appState: AppState = {
    tokenInfo: {
        token: '',
        id_token: ''
    },
    user: {
        email: '',
        family_name: '',
        given_name: '',
        picture: ''
    }
}

const emptyState: AppState = {
    tokenInfo: {
        token: '',
        id_token: ''
    },
    user: {
        email: '',
        family_name: '',
        given_name: '',
        picture: ''
    }
}

export function appReducer(state = appState, action: Action) {
    switch(action.type) {
        case 'SET_TOKEN_INFO':
            return {
                ...state,
                tokenInfo: action.payload
            }
        case 'SET_USER_DATA':
            return {
                ...state,
                user: action.payload
            }
        case 'CLEAR_DATA':
            return emptyState
        default:
            return state    
    }
}