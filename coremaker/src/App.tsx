import './App.css';
import GoogleLogin from 'react-google-login';
import { useDispatch } from 'react-redux';
import { login } from './store/actions/actions';
import { useNavigate } from 'react-router-dom';
import { GOOGLE_CLIENT_ID, TokenInfo } from './store';

function App() {

  const dispatch = useDispatch()

  const navigate = useNavigate()

  const handleLogin = (googleResponse: any) => {
    const tokenInfo: TokenInfo = {
      token: googleResponse.wc.access_token,
      id_token: googleResponse.wc.id_token
    }
    dispatch(login(tokenInfo))
    navigate('/profile')
  }

  const handleFailuire = (googleResponse: any) => {
    console.log(googleResponse)
  }

  return (
    <div className="App">
      <header className="App-header">
        <div>
          <GoogleLogin
            clientId={GOOGLE_CLIENT_ID}
            buttonText="Login"
            onSuccess={handleLogin}
            onFailure={handleFailuire}
            cookiePolicy={'single_host_origin'}
          >
          </GoogleLogin>
        </div>
      </header>
    </div>
  );
}

export default App;
